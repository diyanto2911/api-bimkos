<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('login','ControllerUsers@login');
$router->post('tambah_bimbingan','ControllerBimbingan@tambahBimbingan');
$router->get('get_bimbingan_guru/{idSekolah}','ControllerBimbingan@getBimbinganByGuru');
$router->get('get_bimbingan/{nis}','ControllerBimbingan@getBimbingan');
$router->post('update_status_bimbingan/{id}','ControllerBimbingan@updateStatusBimbingan');
$router->get('get_guru_bk/{id}','ControllerUsers@getGuruBk');
$router->post('update_status','ControllerUsers@updateStatus');
$router->get('get_status_siswa/{id}','ControllerUsers@getStatusSiswa');
$router->post('send_chat/{id}','ControllerBimbingan@sendChat');
$router->get('list_chat','ControllerBimbingan@listChat');

