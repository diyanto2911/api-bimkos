<?php

namespace App\Http\Controllers;

use App\Bimbingan;
use App\Chat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ControllerBimbingan extends Controller
{
    //


    public  function tambahBimbingan(Request $request){
        $validator=Validator::make($request->all(),[
           'nis' => 'required',
           'subject' => 'required',
           'isi_bim' => 'required',
           'tgl_bim' => 'required',

        ]);

        if($validator->fails()){
            return response()->json([
               'error' => $validator->errors()->getMessages()
            ]);
        }
        $id=Carbon::now()->format("ymdhms");
        $save=Bimbingan::insertGetId([
                "id_bimbingan" =>  $id,
            "status_by_guru" => "0",

            "nis" => $request->input('nis'),
            "subject" => $request->input('subject'),
            "isi_bim" => $request->input('isi_bim'),
            "tgl_bim" => $request->input('tgl_bim'),
            "keterangan_bim" => 'aktif',
            "status" => "0",
            "timestamps" => $request->input('timestamps'),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
            "id_sekolah" => $request->input('id_sekolah')
        ]);

        if(!$save){
            return  response()->json([
               'status' => true,
                'code' => 200,
                'message' => 'data tersimpan',
                'data' => $id
            ]);
        }else{
            return  response()->json([
                'status' => false,
                'code' => 600,
                'message' => 'data gagal tersimpan',
            ]);
        }

    }

    public  function  getBimbingan(Request $request,$nis){
        $data=Bimbingan::with(['detailSiswa'])->where('nis',$nis)->orderBy('created_at','DESC')->get();

        if($data->count()>0){
            return response()->json([
               'status' => true,
               'code' => 200,
               'message' => 'data ditemukan',
               'total_data' => $data->count(),
                'data' => $data
            ]);
        }else{
            return response()->json([
                'status' => false,
                'code' => 600,
                'message' => 'data tidak ditemukan',
                'total_data' => $data->count(),
                'data' => $data
            ]);
        }
    }
    public  function  getBimbinganByGuru(Request $request,$idSekolah){
        $data=Bimbingan::with(['detailSiswa'])->where('id_sekolah',$idSekolah)->orderBy('created_at','DESC')->get();

        if($data->count()>0){
            return response()->json([
               'status' => true,
               'code' => 200,
               'message' => 'data ditemukan',
               'total_data' => $data->count(),
                'data' => $data
            ]);
        }else{
            return response()->json([
                'status' => false,
                'code' => 600,
                'message' => 'data tidak ditemukan',
                'total_data' => $data->count(),
                'data' => $data
            ]);
        }
    }

    public  function  updateStatusBimbingan(Request $request,$id){
        if($request->has("isSiswa")!=null){
            $update=Bimbingan::where('id_bimbingan',$id)->update([
                'status' => "1"
            ]);
        }else{
            $update=Bimbingan::where('id_bimbingan',$id)->update([
                'status_by_guru' => "1"
            ]);
        }

    }

    public  function  sendChat(Request $request,$id){
        $validator=Validator::make($request->all(),[
           'content' => 'required',
            'nik' => 'required',
            'nis' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
               "error" => $validator->errors()->getMessages()
            ]);
        }

        $idMessage=Carbon::now()->format('Ymdhis');
        $simpan=Chat::insertGetId([
            "id_message" => $idMessage,
            "content_message" => $request->input('content'),
            "type_message" => "0",
            "nik" => $request->input('nik'),
            "nis" => $request->input('nis'),
            "id_bimbingan" => $id,
            "sender_id" => $request->input('sender_id'),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        if(!$simpan){
            return  response()->json([
               "status" => true,
               "code" => 200,
                "message" => "message tersimpan"
            ]);
        }

        return  response()->json([
            "status" => false,
            "code" => 600,
            "message" => "message gagal tersimpan"
        ]);

    }

    public  function  listChat(Request $request){
        $chat=Chat::with(['detailSiswa','detailGuru'])->where('id_bimbingan',$request->id_bimbingan)->get();

        return response()->json([
           'status' => true,
           'code' => 200,
            'message' => 'data ditemukan',
            'data' => $chat
        ]);
    }

}
