<?php

namespace App\Http\Controllers;

use App\Guru;
use App\Siswa;
use App\Users;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ControllerUsers extends Controller
{


    //

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'nis' => 'required|integer',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()->getMessages()
            ]);
        }

        $cekUser = Siswa::join('jurusan', 'jurusan.id_jurusan', 'siswa.id_jurusan')
            ->join('kelas', 'kelas.id_kelas', 'siswa.id_kelas')
            ->join('sekolah', 'sekolah.id_sekolah', 'siswa.id_sekolah')
            ->join('tingkatan', 'tingkatan.id_tingkatan', 'siswa.id_tingkatan')
            ->where('siswa.nis', $request->input('nis'))->first();
//        dd($cekUser);
        if ($cekUser) {

            $siswa = Users::where('id_user', $cekUser->id_user)->first();

            if ($siswa) {
                if ($request->input('password') == "siswa2020") {
                    return response()->json([
                        'status' => true,
                        'code' => 200,
                        'message' => 'login berhasil',
                        'data' => [
                            'role' => 'siswa',
                            'detail_data' => $cekUser
                        ]
                    ]);
                } else {
                    return response()->json([
                        'status' => false,
                        'code' => 600,
                        'message' => 'login gagal',

                    ]);
                }
            } else {
                return response()->json([
                    'status' => false,
                    'code' => 600,
                    'message' => 'login gagal',

                ]);
            }
        } else {
            $cekUser = Guru::join('jabatan', 'jabatan.id_jabatan', 'guru.id_jabatan')
                ->join('sekolah', 'sekolah.id_sekolah', 'guru.id_sekolah')->where('nik', $request->input('nis'))->first();
            if ($cekUser) {
                $guru = Users::where('id_user', $cekUser->id_user)->first();
                if ($guru) {
                    if ($request->input('password') == "guru2020") {
                        return response()->json([
                            'status' => true,
                            'code' => 200,
                            'message' => 'login berhasil',
                            'data' => [
                                'role' => 'guru',
                                'detail_data' => $cekUser
                            ]
                        ]);
                    } else {
                        return response()->json([
                            'status' => false,
                            'code' => 600,
                            'message' => 'login gagal',

                        ]);
                    }
                } else {
                    return response()->json([
                        'status' => false,
                        'code' => 600,
                        'message' => 'login gagal',

                    ]);
                }
            } else {
                return response()->json([
                    'status' => false,
                    'code' => 600,
                    'message' => 'login gagal',

                ]);
            }
        }


    }

    public function getGuruBk(Request $request, $id)
    {
        $cekData = Guru::
        join('tb_users','tb_users.id_user','guru.id_user')->
        join('jabatan', 'jabatan.id_jabatan', 'guru.id_jabatan')
            ->where('jabatan.nama_jabatan','Guru BK')->where('id_sekolah', $id)->get();

      return  response()->json([
           'status' => true,
           'code' =>200,
           'message' => 'data ditemukan',
           'data' => $cekData
        ]);

    }

    public function getStatusSiswa(Request $request, $id)
    {
        $statusSiswa = Siswa::
        join('tb_users','tb_users.id_user','siswa.id_user')->where('tb_users.id_user', $id)->get();

        return response()->json([
           'status' => true,
           'code' => 200,
           'message' => 'data ditemukan',
           'data' => $statusSiswa
        ]);
    }

    public  function  updateStatus(Request $request){
        $validation=Validator::make($request->all(),[
           'id_user' => 'required',
        ]);

        if($validation->fails()){
            return response()->json([
               "error"  => $validation->errors()->getMessages()
            ]);
        }

        $update=Users::where('id_user',$request->id_user)
            ->update([
               "status" => $request->input('status'),
                "last_seen" => Carbon::now(),
                "updated_at" => Carbon::now()
            ]);

        if($update){
            return  response()->json([
               'status' => true,
                'code' => 200,
                'message' => 'data ter-update'
            ]);
        }
        return  response()->json([
            'status' => false,
            'code' => 600,
            'message' => 'data gagal ter-update'
        ]);
    }

}
