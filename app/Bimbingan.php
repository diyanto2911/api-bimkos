<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bimbingan extends Model
{
    //

    protected $table='bimbingan';

    public  function  detailSiswa(){
       return $this->belongsTo(Siswa::class,'nis','nis');
    }
}
