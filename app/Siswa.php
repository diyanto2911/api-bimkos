<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    //

    protected $table='siswa';

    public  function  detailJurusan(){
        $this->hasOne(Jurusan::class, 'id_jurusan','id_jurusan');
    }
}
