<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    //

    protected  $table='tb_chat';

    public  function  detailGuru(){
      return  $this->hasOne(Guru::class, 'nik','nik');
    }

    public  function  detailSiswa(){
        return $this->hasOne(Siswa::class, 'nis','nis');
    }
}
